package model.vo;

import com.opencsv.bean.CsvBindByName;



/**
 * Representation of a Trip object
 */
public class VOMovingViolations {



	@CsvBindByName
	private String row_;
	
	@CsvBindByName(column = "OBJECTID")
	private int objectid;

	@CsvBindByName
	private String location;

	@CsvBindByName(column = "ADDRESS_ID")
	private String address_id;

	@CsvBindByName(column = "STREETSEGID")
	private String streetsegid;

	@CsvBindByName(column = "XCOORD")
	private String xcoord;

	@CsvBindByName(column = "YCOORD")
	private String ycoord;

	@CsvBindByName(column = "TICKETTYPE")
	private String tickettype;

	@CsvBindByName(column = "FINEAMT")
	private String fineamt;

	@CsvBindByName(column = "TOTALPAID")
	private int totalpaid;

	@CsvBindByName(column = "PENALTY1")
	private String penalty1;

	@CsvBindByName(column = "PENALTY2")
	private String penalty2;

	@CsvBindByName(column = "ACCIDENTINDICATOR")
	private String accidentindicator;

	@CsvBindByName(column = "TICKETISSUEDAY")
	private String ticketissueday;

	@CsvBindByName(column = "VIOLATIONCODE")
	private String violationcode;

	@CsvBindByName(column = "VIOLATIONDESC")
	private String violationdesc;

	@CsvBindByName(column = "ROW_ID")
	private String row_id;

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getobjectid() {
		return objectid;
	}

	/**
	 * 
	 */
	public String getRow_() {
		return row_;
	}

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * 
	 * @return
	 */
	public String getAddress_id()
	{
		return address_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getStreetsegid()
	{
		return streetsegid;
	}

	/**
	 * 
	 * @return
	 */
	public String getXcoord()
	{
		return xcoord;
	}

	/**
	 * 
	 * @return
	 */
	public String getYcoord()
	{
		return ycoord;
	}

	/**
	 * 
	 * @return
	 */
	public String getTickettype()
	{
		return tickettype;
	}

	/**
	 * 
	 * @return
	 */
	public String getFineamt()
	{
		return fineamt;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		return totalpaid;
	}

	/**
	 * 
	 * @return
	 */
	public String getPenalty1()
	{
		return penalty1;
	}

	/**
	 * 
	 * @return
	 */
	public String getPenalty2()
	{
		return penalty2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentindicator;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return ticketissueday;
	}

	/**
	 * 
	 * @return
	 */
	public String  getViolationcode() {
		return violationcode;
	}

	/**
	 * 
	 * @return
	 */
	public String getrow_id()
	{
		return row_id;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDesc() {
		return violationdesc;
	}


	/**
	 * @return id - Identificador único de la infracción
	 */
	public void setobjectid(int objectId) {
		objectid = objectId;
	}	

	/**
	 * 
	 */
	public void setRow_(String r) {
		row_ = r;
	}

	/**
	 * 
	 * @param ad
	 */
	public void setAddress_id(String ad)
	{
		address_id = ad;
	}


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public void setLocation(String loc) {
		location = loc;
	}

	/**
	 * 
	 * @param ssd
	 */
	public void setStreetsegid( String ssd)
	{
		streetsegid = ssd;
	}

	/**
	 * 
	 * @param x
	 */
	public void setXcoord(String x)
	{
		xcoord = x;
	}

	/**
	 * 
	 * @param y
	 */
	public void setYcoord(String y)
	{
		ycoord = y;
	}

	/**
	 * 
	 * @return
	 */
	public void setTickettype(String tt)
	{
		tickettype = tt;
	}

	/**
	 * 
	 * @return
	 */
	public void setFineamt( String fa)
	{
		fineamt = fa;
	}


	/**
	 * 
	 * @param tp
	 */
	public void setTotalPaid(int tp) {
		totalpaid= tp;
	}

	/**
	 * 
	 * @param p1
	 */
	public void getPenalty1(String p1)
	{
		penalty1= p1;
	}

	/**
	 * 
	 * @param p2
	 */
	public void setPenalty2(String p2)
	{
		penalty2= p2;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public void setTicketIssueDate(String tid) {
		ticketissueday = tid;
	}


	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public void  setAccidentIndicator(String ai) {
		accidentindicator = ai;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public void  setViolationDes(String vd) {
		violationdesc = vd;
	}

	/**
	 * 
	 * @param ri
	 */
	public void setRow_id(String ri)
	{
		row_id = ri;
	}


	@Override
	public String toString() {

		return objectid + row_  + " | "+ address_id  + " | "+ xcoord + " | "+ ycoord + " | " + location + " | "+ violationdesc + "\n";
	}
}
