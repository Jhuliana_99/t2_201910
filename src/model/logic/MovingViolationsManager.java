package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager {

	/**
	 * Lista encadenada
	 */
	private LinkedList<VOMovingViolations> linkedlist;

	/**
	 * 
	 */
	public void loadMovingViolations(String movingViolationsFile){

		Path myPath = Paths.get( movingViolationsFile);
		try (BufferedReader br = Files.newBufferedReader(myPath)){


			HeaderColumnNameMappingStrategy<VOMovingViolations> strategy = new HeaderColumnNameMappingStrategy<>();
			strategy.setType(VOMovingViolations.class);

			CsvToBean csvToB = new CsvToBeanBuilder(br).withType(VOMovingViolations.class).withMappingStrategy(strategy).withIgnoreLeadingWhiteSpace(true).build();

			linkedlist = new LinkedList<VOMovingViolations>();

			for(Iterator<VOMovingViolations> i = csvToB.parse().iterator();i.hasNext(); ){

				linkedlist.add(i.next());
			}

		}

		catch (FileNotFoundException e1) {

		} catch (IOException e) {

		}
	}


	@Override
	public LinkedList<VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {

		LinkedList<VOMovingViolations> rta = new LinkedList<VOMovingViolations>();
		loadMovingViolations("./data/Moving_Violations_Issued_in_January_2018.csv");
		Iterator<VOMovingViolations> iter = linkedlist.iterator();

		while(iter.hasNext())
		{
			if(iter.next().getViolationcode().equals(violationCode))
			{
				rta.add(iter.next());
			}
		}
		return rta;

	}

	@Override
	public LinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {

		LinkedList<VOMovingViolations> rta = new LinkedList<VOMovingViolations>();
		loadMovingViolations("./data/Moving_Violations_Issued_in_January_2018.csv");
		Iterator<VOMovingViolations> iter = linkedlist.iterator();

		while(iter.hasNext())
		{
			if(iter.next().getViolationcode().equals(accidentIndicator))
			{
				rta.add(iter.next());
			}
		}
		return rta;
	}	


}
