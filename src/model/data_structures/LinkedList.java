package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T>{


	/**
	 * El primer nodo a agregar a la lista
	 */
	private Node<T> node1;

	/**
	 * El �ltimo nodo de la lista
	 */
	private Node<T> last;

	/**
	 * Representa el tama�o actual de la lista
	 */
	private int tama�oAct;

	/**
	 * 
	 */
	private Node<T> currentElement;


	//Constructor//

	public LinkedList()
	{
		node1 = null;
		last = null;
		tama�oAct = -1;
		currentElement = null;
	}


	/**
	 * Iterador de la lista
	 */
	public Iterator<T> iterator() {

		Iterator<T> t = new Iterator<T>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {
				return currentIndex < getSize() && getCurrentElement() != null;
			}

			@Override
			public T next() {
				Node<T> h = currentElement;
				currentElement = h.darNext();
				currentIndex++;
				return currentElement.darElemento();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return t;
	}


	/**
	 * Retorna un integer con el tamabo actual de la lista
	 */
	public Integer getSize() {
		return tama�oAct + 1;
	}

	/**
	 * Agrega un elemento al comienzo de la lista
	 * @param element, elemento a ser agregado
	 */
	public void add(T element)
	{
		Node<T> g = new Node<T>(element);
		tama�oAct++;

		if(node1== null){
			node1 = g;
			last = g;
		}
		else{
			g.changeNext(node1);
			node1 = g;
		}
		currentElement = g;

	}

	/**
	 * Agrega un elemento gen�rico al final de la lista
	 * @param element, elemento a ser agregado
	 */
	public void addAtEnd(T element)
	{
		Node<T> g = new Node<T>(element);

		if(node1 == null)
		{
			last = g;
			node1 = g;
			currentElement = node1;
		}
		else{
			last.changeNext(g);
			g.changePrev(last);
			last = g;
		}
		tama�oAct++;
	}

	/**
	 * Agrega un elemento en el �ndice dado. EL tama�o de la lista aumenta en uno.
	 * @param k, el �ndice donde se quiere agregar el elemento.
	 * @param element, el elemento que se quiere agregar.
	 */
	public void addAtK(int k, T element)
	{

		if(k ==0)
		{
			add(element);
		}

		else{
			Iterator<T> t = iterator();
			int cont = 0;
			T prv = null;
			T nxt = null;

			while(t.hasNext() && cont < k)
			{
				prv = (T) t;
				nxt = (T)t.next();
				cont++;
			}

			if(nxt != null)
			{
				Node<T> yy = new Node<T>(element);
				Node<T> p = new Node<T>(prv);
				Node<T> n = new Node<T>(nxt);

				p.changeNext(yy);
				n.changePrev(yy);
				yy.changeNext(n);
				yy.changePrev(p);
			}
			else{
				addAtEnd(element);
			}
		}
		tama�oAct++;

	}

	/**
	 * BUsca el elemento en el �ndice dado y lo retorma
	 * @param index, el �ndice dado
	 */
	public T getElement(int index)
	{
		T rta = null;

		if(index == 0)
		{
			rta = node1.darElemento();
		}
		else if(index == tama�oAct)
		{
			rta = last.darElemento();
		}
		else{
			int cont =0;
			Iterator iter = iterator();
			T f = null;

			while(iter.hasNext() && cont < index)
			{
				f = (T)iter.next();
				cont++;
			}
			rta = f;

		}

		return rta;
	}

	/**
	 * Busca el elemento dado por parametro y lo elimina
	 * @param el, es el elemento a eliminar
	 */
	public void delete(T el)
	{
		Node<T> x = node1;
		if(x.equals(new Node<T>(el)))
		{
			Node<T> sig = x.darNext();
			sig.changePrev(null);
			node1.changeNext(null);
			node1 = sig;
			tama�oAct--;

		}
		else{
			while(x.darNext()!= null)
			{
				if(x.darNext().equals(new Node<T>(el)))
				{
					Node<T> ant = x;
					Node<T> sig2 = (x.darNext()).darNext();
					if(sig2 !=null){
						ant.changeNext(sig2);
						sig2.changePrev(x);
					}
					else{
						ant.changeNext(null);
					}
					(x.darNext()).changeNext(null);
					(x.darNext()).changePrev(null);
					tama�oAct--;
				}
				else{
					x = x.darNext();
				}
			}
		}
	}


	@Override
	public T deleteAtK(int k) {

		T rta = null;
		if(k ==0)
		{
			Node<T> kk = node1.darNext();
			Node<T> eliminar = node1;
			node1.changeNext(null);
			kk.changePrev(null);
			node1= kk;
			rta = eliminar.darElemento();
		}

		else{
			Iterator<T> t = iterator();
			int cont = 0;
			Node<T> prv = null;
			Node<T> nxt = null;
			Node<T> act2 = null;
			Iterator act = null;

			while(t.hasNext() && cont < k)
			{
				prv = new Node<T>((T)t);
				act2 = new Node<T>((T)t.next());
				act = (Iterator) t.next();
				nxt = new Node<T>((T)act.next());
				cont++;
			}

			if(nxt != null)
			{
				prv.changeNext(nxt);
				nxt.changePrev(prv);
				act2.changeNext(null);
				act2.changePrev(null);
				rta = act2.darElemento();
			}

			else{
				prv.changeNext(null);
				act2.changePrev(null);
				last = prv;
				rta = act2.darElemento();

			}
		}
		tama�oAct--;
		return rta;
	}


	@Override
	public T next() {
		return currentElement.darNext().darElemento();
	}


	@Override
	public T previous() {
		return currentElement.darPrev().darElemento();
	}


	@Override
	public T getCurrentElement() {
		return currentElement.darElemento();
	}





}
