

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import junit.framework.TestCase;

public class LinkedListTest extends TestCase
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Es la clase donde se har�n las pruebas
	 */
	private LinkedList<String> listaprueba;

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Construye una nueva lista encadenada
	 */
	private void setupEscenario1( )
	{
		listaprueba = new LinkedList<String>();

		listaprueba.add("aaaa0");
		listaprueba.add("bbbb1");
		listaprueba.add("cccc2");
		listaprueba.add("dddd3");
		listaprueba.add("eeee4");
		listaprueba.add("ffff5");
		listaprueba.add("gggg6");
		listaprueba.add("hhhh7");
		listaprueba.add("iiii8");
		listaprueba.add("jjjj9");


	}


	/**
	 * Prueba el m�todo add
	 */
	public void testAdd( )
	{
		setupEscenario1( );

		Iterator<String> iter = (Iterator) listaprueba.iterator();

		ArrayList list = new ArrayList();
		while(iter.hasNext())
		{
			list.add(iter.next());
		}

		try
		{
			listaprueba.add("kkk10");

			boolean found = false;
			ArrayList list2 = new ArrayList();
			
			while(iter.hasNext())
			{
				list2.add(iter.next());
			}
			
			for( int i = 0; i < list2.size( ); i++ )
			{
				String c = (String) list2.get( i );
				if( c.equals("kkk10") )
					found = true;
			}

			assertTrue( "No se encontr� el nuevo elemento agregado", found );
			assertEquals("El primer elemento de la lista debe ser el agregado", list2.get(0), "kkk10");

			assertEquals( "El n�mero de elementos no es el correcto", list.size() + 1, list2.size( ) );
		}
		catch( Exception e )
		{
			fail( "No deber�a fallar" );
		}
	}
}

//
//	/**
//	 * Prueba el m�todo para eliminar una ciudad. <br>
//	 * <b> M�todos a probar: </b> <br>
//	 * eliminarCiudad. <br>
//	 * <b> Objetivo: </b> probar que el m�todo eliminarCiudad() elimina correctamente las ciudades de la aerol�nea. <br>
//	 * <b> Resultados esperados: </b> <br>
//	 * 1. Al eliminar una ciudad existente, este debe desaparecer de la lista de ciudades.
//	 */
//	public void testEliminarCiudad( )
//	{
//		setupEscenario1( );
//
//		ArrayList ciudades = aerolinea.darCiudades( );
//		Ciudad ciudadEliminada = ( Ciudad )ciudades.get( 1 );
//		try
//		{
//			// Elimina una ciudad del medio
//			aerolinea.eliminarCiudad( ciudadEliminada.darNombre( ) );
//			ArrayList ciudades2 = aerolinea.darCiudades( );
//			assertEquals( "El n�mero de ciudades no es correcto", ciudades.size( ) - 1, ciudades2.size( ) );
//			for( int i = 0; i < ciudades2.size( ); i++ )
//			{
//				Ciudad c = ( Ciudad )ciudades2.get( i );
//				assertFalse( c.darNombre( ).equals( ciudadEliminada.darNombre( ) ) );
//			}
//		}
//		catch( AerolineaExcepcion e )
//		{
//			fail( "No deber�a fallar nada" );
//		}
//
//		ciudadEliminada = ( Ciudad )ciudades.get( 0 );
//
//		try
//		{
//			// Elimina la primera ciudad de la lista
//			aerolinea.eliminarCiudad( ciudadEliminada.darNombre( ) );
//
//			ArrayList ciudades2 = aerolinea.darCiudades( );
//			assertEquals( "El n�mero de ciudades no es correcto", ciudades.size( ) - 2, ciudades2.size( ) );
//			for( int i = 0; i < ciudades2.size( ); i++ )
//			{
//				Ciudad c = ( Ciudad )ciudades2.get( i );
//				assertFalse( c.darNombre( ).equals( ciudadEliminada.darNombre( ) ) );
//			}
//		}
//		catch( AerolineaExcepcion e )
//		{
//			fail( "No deber�a fallar nada" );
//		}
//
//		ciudadEliminada = ( Ciudad )ciudades.get( 3 );
//
//		try
//		{
//			// Elimina la ultima ciudad de la lista
//			aerolinea.eliminarCiudad( ciudadEliminada.darNombre( ) );
//
//			ArrayList ciudades2 = aerolinea.darCiudades( );
//			assertEquals( "El n�mero de ciudades no es correcto", ciudades.size( ) - 3, ciudades2.size( ) );
//			for( int i = 0; i < ciudades2.size( ); i++ )
//			{
//				Ciudad c = ( Ciudad )ciudades2.get( i );
//				assertFalse( c.darNombre( ).equals( ciudadEliminada.darNombre( ) ) );
//			}
//		}
//		catch( AerolineaExcepcion e )
//		{
//			fail( "No deber�a fallar nada" );
//		}
//
//	}
//
//	/**
//	 * Prueba el m�todo para eliminar una ciudad. <br>
//	 * <b> M�todos a probar: </b> <br>
//	 * eliminarCiudad. <br>
//	 * <b> Objetivo: </b> probar que el m�todo eliminarCiudad() arroja excepci�n al tratar de eliminar una ciudad que no exista. <br>
//	 * <b> Resultados esperados: </b> <br>
//	 * 1. Al tratar de eliminar una ciudad inexistente se debe arrojar excepci�n.
//	 */
//	public void testEliminarCiudadError( )
//	{
//		setupEscenario1( );
//		ArrayList ciudades1 = aerolinea.darCiudades( );
//
//		try
//		{
//			aerolinea.eliminarCiudad( "La ciudad" );
//			fail( "No deber�a haber eliminado la ciudad" );
//		}
//		catch( AerolineaExcepcion e )
//		{
//			// Se verifica que el n�mero de ciudades no se haya alterado
//			ArrayList ciudades2 = aerolinea.darCiudades( );
//			assertEquals( "El n�mero de ciudades no deber�a haber cambiado", ciudades1.size( ), ciudades2.size( ) );
//
//		}
//	}
//}